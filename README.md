# Orchestrator

The crux of this project aims to provide a single place where all deployments can commence and be orchestrated. 

A few perks of this project

The [Trigger Job YAML](https://gitlab.com/regrabneffop-ultimate/experiments/orchestrator/-/blob/main/.gitlab-ci.yml#L11-25) leverages GitLab-native offerings - and the [downstream pipeline names get renamed](https://gitlab.com/regrabneffop-ultimate/experiments/orchestrated/project-a/-/pipelines) to incorporate which environment it was deployed to.

##### Environment Dashboard (by environment tier):

The gap in this approach is that the downstrem "tags" cannot be seen.

1. [Dev Deploys](https://gitlab.com/regrabneffop-ultimate/experiments/orchestrator/-/environments/folders/dev)
2. [Staging Deploys](https://gitlab.com/regrabneffop-ultimate/experiments/orchestrator/-/environments/folders/staging)

or by Application Name

1. [Project A Deploys](https://gitlab.com/regrabneffop-ultimate/experiments/orchestrator/-/environments?page=1&scope=active&search=project-a)
1. [Project B Deploys](https://gitlab.com/regrabneffop-ultimate/experiments/orchestrator/-/environments?page=1&scope=active&search=project-b)

### Settings & CI Variables Required

1. Group Access Token with API Access to the Orchestrated group, stored as `PRIVATE_TOKEN` in the CI Variables and used in the `aggregate.py` to find newly created tags so that they can be added to the `versions.yml` file
1. Project Access Token with API Access to this Project, stored as `PROJECT_TOKEN` in the CI Variables and used in the `aggregate.py` to push changes of the `versions.yml` file
1. Projects in the Orchestrated group have allowed this project `Token Access` in the CI Settings so that this project can initiate downstream pipelines.
1. Scheduled pipeline that runs to continuously check for new tags created in all of the "Orchestrated" projects. This could easily be solved with a webhook or other listener


### Nice to Haves at some point...

1. Auto-update the "default" to the newest, undeployed tag and then automatically set it back to `none` when the deployment succeeds. This could be done in a `.post` CI Job that in this project that evaluates all jobs and determine which environments were deployed into, by looking at the variables defined in the `versions.yml` file and then seeing if they exist in the pipeline. For any job that succeeded, assume that the deployment was successful and then remove that from the default. 