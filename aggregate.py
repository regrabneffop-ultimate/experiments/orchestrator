import os
import requests
import yaml
from envparse import Env
from pprint import pprint

try: 
    from envparse import Env        
    env = Env()
    if os.path.isfile('.env'):
        env.read_envfile('.env')
except ImportError:
    env = os.getenv

group_id = '82882597'

def push_changes():
    """
    PUT /projects/:id/repository/files/:file_path
    """
    project_id = env('CI_PROJECT_ID')

    with open('versions.yml', 'r') as f:
        content = f.read()

    origin = env('CI_API_V4_URL')
    payload = {
        "branch": "main", 
        "author_email": "tpoffenbarger@gitlab.com", 
        "author_name": "Tim Poffenbarger",
        "content": content, 
        "commit_message": "Add new files [skip-ci]",
    }

    r = requests.put(f'{origin}/projects/{project_id}/repository/files/versions.yml', json=payload, headers={'PRIVATE-TOKEN': env('PROJECT_TOKEN')})
    if not r.ok:
        pprint(r.json())



def main():
    """
    go through every project and add any new tags to the list of variables
    """
    print('Going through every project and add any new tags to the list of variables in the CI YAML')
    origin = env('CI_API_V4_URL')

    url = f'{origin}/groups/{group_id}/projects'
    headers = {'PRIVATE-TOKEN': env('PRIVATE_TOKEN')}
    response = requests.get(url, headers=headers)
    if os.path.isfile('versions.yml'):
        with open('versions.yml', 'r') as f:
            ci = yaml.safe_load(f)
        print(ci['variables'])
    write_file = False
    for project in response.json():
        project_name = project["name"]
        project_slug = project["path"].upper().replace('-', '_')
        print(f'Project: {project_slug}')
        tag_url = f'{origin}/projects/{project["id"]}/repository/tags'
        tag_response = requests.get(tag_url, headers=headers)
        for tag in tag_response.json():
            added = False
            if tag['name'] in ci['variables'].get(f'{project_slug}_VERSION', {}).get('options', []):
                print(f'{tag["name"]}: Added!')
                added = True
            else:
                print(f'Adding Tag: {tag["name"]} because it is not in the list for {project_slug}_VERSION:', ci['variables'].get(f'{project_slug}_VERSION', {}).get('options', []))
                ci['variables'][f'{project_slug}_VERSION']['options'].append(tag['name'])
                print(ci['variables'][f'{project_slug}_VERSION']['options'])
                write_file = True
        
        # Go ahead and sort the list appropriately
        
        final_list = list(reversed(sorted(ci['variables'].get(f'{project_slug}_VERSION', {}).get('options', []))))
        if 'none' in final_list:
            final_list.remove('none')
        final_list = ['none'] + final_list
        if ci['variables'].get(f'{project_slug}_VERSION', {}).get('options', []) != final_list:
            print(f'Updating {project_slug}_VERSION to {final_list}')
            ci['variables'][f'{project_slug}_VERSION']['options'] = final_list
            write_file = True
    if write_file:
        print('\nSaving file.')
        print(ci['variables'])
        with open('versions.yml', 'w') as f:
            yaml.dump(ci, f)
        push_changes()


if __name__ == '__main__':
    main()
